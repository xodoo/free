# -*- coding: utf-8 -*-
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
# odoo16
# QQ:35350428
# 邮件:35350428@qq.com
# 手机：13584935775
# 作者：'Amos'
# 公司网址： www.xodoo.cn
# Copyright 昆山一百计算机有限公司
# 日期：2023-09-16
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

{
    'name': "资源库",
    'summary': """资源库""",
    'author': "Amos",
    'website': "www.xodoo.cn",
    'category': '中台应用/风格',
    'sequence': 10,
    "application": True,
    'version': '0.1',
    "depends": ['web'],
    'price': '200',
    'currency': 'CNY',
    'data': [
    ],
    'description': """
    菜单侧边单显示
""",
    'application': False,
    'auto_install': False,
    'license': 'LGPL-3',
}
